'''
This file contains the functions from tda_functions.py that will be tested.
'''
import numpy as np
import scipy.spatial.distance as distance
from Topological_ML import tda_function as tda

def test_lens_1d_pass():
    '''
    Testing whether the function works given correct parameters.
    '''
    data = np.ones((5, 2))
    test, _ = tda.lens_1d(data, proj="sum")
    assert isinstance(test, np.ndarray)

def test_lens_1d_fail_1():
    '''
    Testing whether the function fails given incorrect projection parameter.
    '''
    data = np.ones((5, 2))
    test, _ = tda.lens_1d(data, proj="Sum")
    assert test is None

def test_lens_1d_fail_2():
    '''
    Testing whether the function fails given incorrect feature (data) parameter.
    '''
    data = np.ones(5)
    test, _ = tda.lens_1d(data, proj="sum")
    assert test is None

def test_uniform_sampling_pass():
    '''
    Testing whether the function works given correct parameters.
    '''
    x_array = np.array([[1, 1], [1, 2], [2, 3]])
    dm_x = distance.cdist(x_array, x_array)
    test, _ = tda.uniform_sampling(dm_x, 1)
    assert isinstance(test, np.ndarray)

def test_uniform_sampling_fail_1():
    '''
    Testing whether the function fails given incorrect distance matrix parameter.
    '''
    x_array = np.ones(5)
    test, _ = tda.uniform_sampling(x_array, 1)
    assert test is None

def test_uniform_sampling_fail_2():
    '''
    Testing whether the function fails given incorrect sampling size parameter.
    '''
    x_array = np.array([[1, 1], [1, 2], [2, 3]])
    dm_x = distance.cdist(x_array, x_array)
    test, _ = tda.uniform_sampling(dm_x, -2)
    assert test is None
